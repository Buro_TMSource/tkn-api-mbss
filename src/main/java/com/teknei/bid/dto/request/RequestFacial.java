package com.teknei.bid.dto.request;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Amaro on 06/07/2017.
 */
@Data
public class RequestFacial implements Serializable {

    private String id;
    private String facial;

}