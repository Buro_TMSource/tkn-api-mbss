package com.teknei.bid.dto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.Data;
import org.springframework.util.Base64Utils;

import com.morpho.util.WSHelper;

import java.io.Serializable;

/**
 * Created by Amaro on 06/07/2017.
 */
@Data
public class FingersBytes implements Serializable {
	private static final Logger log = LoggerFactory.getLogger(FingersBytes.class);
    public FingersBytes(Fingers fingers) {
    	//log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".FingerBytes ");
        if (fingers.getLt() != null && !fingers.getLt().isEmpty()) {
            lt = Base64Utils.decodeFromString(fingers.getLt());
        }
        if (fingers.getLi() != null && !fingers.getLi().isEmpty()) {
            li = Base64Utils.decodeFromString(fingers.getLi());
        }
        if (fingers.getLm() != null && !fingers.getLm().isEmpty()) {
            lm = Base64Utils.decodeFromString(fingers.getLm());
        }
        if (fingers.getLr() != null && !fingers.getLr().isEmpty()) {
            lr = Base64Utils.decodeFromString(fingers.getLr());
        }
        if (fingers.getLl() != null && !fingers.getLl().isEmpty()) {
            ll = Base64Utils.decodeFromString(fingers.getLl());
        }
        if (fingers.getRt() != null && !fingers.getRt().isEmpty()) {
            rt = Base64Utils.decodeFromString(fingers.getRt());
        }
        if (fingers.getRi() != null && !fingers.getRi().isEmpty()) {
            ri = Base64Utils.decodeFromString(fingers.getRi());
        }
        if (fingers.getRm() != null && !fingers.getRm().isEmpty()) {
            rm = Base64Utils.decodeFromString(fingers.getRm());
        }
        if (fingers.getRr() != null && !fingers.getRr().isEmpty()) {
            rr = Base64Utils.decodeFromString(fingers.getRr());
        }
        if (fingers.getRl() != null && !fingers.getRl().isEmpty()) {
            rl = Base64Utils.decodeFromString(fingers.getRl());
        }
    }

    private byte[] lt;
    private byte[] li;
    private byte[] lm;
    private byte[] lr;
    private byte[] ll;
    private byte[] rt;
    private byte[] ri;
    private byte[] rm;
    private byte[] rr;
    private byte[] rl;

}
