package com.teknei.bid.controller.rest;

import com.google.common.hash.Hashing;
import com.morpho.mbss.generic.wsdl.FingerprintTemplates;
import com.morpho.mbss.generic.wsdl.Person;
import com.teknei.bid.dto.HttpStatusImageWrapper;
import com.teknei.bid.dto.HttpStatusWrapper;
import com.teknei.bid.dto.request.*;
import com.teknei.bid.dto.response.BasicResponse;
import com.teknei.bid.dto.response.ComplexResponse;
import com.teknei.bid.dto.response.SimpleResponse;
import com.teknei.bid.ws.util.MbssUtils;
import io.swagger.annotations.ApiOperation;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Amaro on 06/07/2017.
 */
@RestController
@RequestMapping(value = "/mbss")
public class MBSSController {

    private static final Logger log = LoggerFactory.getLogger(MBSSController.class);

    //@Autowired
    //private WSInvoker wsInvoker;
    @Autowired
    private MbssUtils mbssUtils;


    /**
     * Creates a resource in biometric system based on request values
     *
     * @param requestCreate
     * @return the response wrapper values
     */
    @ApiOperation(value = "Create a resource in biometric system. Type to send: {1 - Fingers Only | 2 - Facial Only | 3 - Fingers + Facial  " +
            " | 4 - Slaps Only | 6 - Slaps + Facial }")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = "application/json")
    public ResponseEntity<SimpleResponse> createRecord(@RequestBody RequestCreate requestCreate)
    {
        log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".createRecord ");
        try 
        {
            Files.write(Paths.get("/home/archivo_json.json"), requestCreate.toString().getBytes());
        } catch (IOException e) {
            log.error("Error writting income file: {}", e.getMessage());
        }
        String typeRequested = requestCreate.getType();
        String id = requestCreate.getId();
        int typeReq = 0;
        if (typeRequested == null) {
            log.info("Returning: {} - {}", buildResponseMissingFields(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(buildResponseMissingFields(), HttpStatus.BAD_REQUEST);
        }
        try {
            typeReq = Integer.parseInt(typeRequested);
        } catch (NumberFormatException ne) {
            log.info("Returning: {} - {}", buildResponseMissingFields(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(buildResponseMissingFields(), HttpStatus.BAD_REQUEST);
        }
        HttpStatus responseStatus;
        log.info("Type requested: {}", typeReq);
        switch (typeReq) {
            case 1:
                //Fingers request
                responseStatus = mbssUtils.checkAndInsertPersonFromFingers(id, requestCreate.getFingers(), requestCreate.getImageType().toUpperCase());
                break;
            case 2:
                //Face request
                responseStatus = mbssUtils.checkAndInsertPersonFromFace(id, requestCreate.getFacial());
                break;
            case 3:
                //Fingers and face
                responseStatus = mbssUtils.checkAndInsertPersonFromFaceAndFingers(id, requestCreate.getFacial(), requestCreate.getFingers(), requestCreate.getImageType());
                break;
            case 4:
                responseStatus = mbssUtils.checkAndInsertPersonFromSlaps(id, requestCreate.getSlaps());
                break;
            case 6:
                responseStatus = mbssUtils.checkAndInsertPersonFromSlapsAndFace(id, requestCreate.getSlaps(), requestCreate.getFacial());
                break;
            default:
                return new ResponseEntity<>(buildResponseMissingFields(), HttpStatus.BAD_REQUEST);
        }
        SimpleResponse response = new SimpleResponse();
        response.setId(id);
        response.setStatus(responseStatus.toString());
        response.setTime(String.valueOf(System.currentTimeMillis() / 1000));
        log.info("Returning: {} - {}", response, responseStatus);
        return new ResponseEntity<>(response, responseStatus);
    }

    @RequestMapping(value = "/getHash/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getHashForPerson(@PathVariable String id) 
    {
        log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".getHashForPerson ");
        Person person = mbssUtils.findDetailById(id);
        if (person == null) {
            log.info("Person not found");
            return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
        }
        FingerprintTemplates templates = person.getRegistration().get(0).getFingerprintSample().get(0).getTemplates();
        List<String> templateData = new ArrayList<>();
        //Left hand
        if (templates.getLeftLittle() != null) {
            templateData.add(Base64Utils.encodeToString(templates.getLeftLittle().getBuffer()));
        } else {
            log.warn("No data from leftLittle for calculate hash");
        }
        if (templates.getLeftIndex() != null) {
            templateData.add(Base64Utils.encodeToString(templates.getLeftIndex().getBuffer()));
        } else {
            log.warn("No data from leftIndex for calculate hash");
        }
        if (templates.getLeftMiddle() != null) {
            templateData.add(Base64Utils.encodeToString(templates.getLeftMiddle().getBuffer()));
        } else {
            log.warn("No data from leftMiddle for calculate hash");
        }
        if (templates.getLeftRing() != null) {
            templateData.add(Base64Utils.encodeToString(templates.getLeftRing().getBuffer()));
        } else {
            log.warn("No data from leftRing for calculate hash");
        }
        if (templates.getLeftThumb() != null) {
            templateData.add(Base64Utils.encodeToString(templates.getLeftThumb().getBuffer()));
        } else {
            log.warn("No data from leftThumb for calculate hash");
        }
        //Right hand
        if (templates.getRightLittle() != null) {
            templateData.add(Base64Utils.encodeToString(templates.getRightLittle().getBuffer()));
        } else {
            log.warn("No data from rightLittle for calculate hash");
        }
        if (templates.getRightIndex() != null) {
            templateData.add(Base64Utils.encodeToString(templates.getRightIndex().getBuffer()));
        } else {
            log.warn("No data from rightIndex for calculate hash");
        }
        if (templates.getRightMiddle() != null) {
            templateData.add(Base64Utils.encodeToString(templates.getRightMiddle().getBuffer()));
        } else {
            log.warn("No data from rightMiddle for calculate hash");
        }
        if (templates.getRightRing() != null) {
            templateData.add(Base64Utils.encodeToString(templates.getRightRing().getBuffer()));
        } else {
            log.warn("No data from rightRing for calculate hash");
        }
        if (templates.getRightThumb() != null) {
            templateData.add(Base64Utils.encodeToString(templates.getRightThumb().getBuffer()));
        } else {
            log.warn("No data from rightThumb for calculate hash");
        }
        StringBuilder stringBuilder = new StringBuilder();
        templateData.forEach(s -> stringBuilder.append(s));
        String hashTemplate = Hashing.sha256().hashString(stringBuilder.toString(), StandardCharsets.UTF_8).toString();
        String md5 = Hashing.md5().hashString(hashTemplate, StandardCharsets.UTF_8).toString();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("hashRaw", md5);
        jsonObject.put("hash", String.valueOf(md5.hashCode()));
        return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
    }

    @RequestMapping(value = "/searchDetail/{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<ComplexResponse> findDetailRecord(@PathVariable String id) {
        log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findDetailRecord ");
        Person person = mbssUtils.findDetailById(id);
        if (person == null) {
            return new ResponseEntity<>((ComplexResponse) null, HttpStatus.NOT_FOUND);
        }
        try {
            ComplexResponse complexResponse = new ComplexResponse();
            complexResponse.setId(id);
            String leftIndex = null;
            String rightIndex = null;
            byte[] leftIndexBuffer = null;
            try {
                leftIndexBuffer = person.getRegistration().get(0).getFingerprintSample().get(0).getTemplates().getLeftIndex().getBuffer();
                if (leftIndexBuffer != null) {
                    leftIndex = Base64Utils.encodeToString(leftIndexBuffer);
                }
                complexResponse.setLeftIndex(leftIndex);
            } catch (NullPointerException e) {
                log.error("No left index found");
            }
            byte[] rightIndexBuffer = null;
            try {
                rightIndexBuffer = person.getRegistration().get(0).getFingerprintSample().get(0).getTemplates().getRightIndex().getBuffer();
                if (rightIndexBuffer != null) {
                    rightIndex = Base64Utils.encodeToString(rightIndexBuffer);
                }
                complexResponse.setRightIndex(rightIndex);
            } catch (Exception e) {
                log.error("No right index found");
            }
            return new ResponseEntity<>(complexResponse, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding complex detail for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>((ComplexResponse) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @RequestMapping(value = "/search/{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<BasicResponse> findRecord(@PathVariable String id) 
    {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findRecord ");
        HttpStatus response = mbssUtils.findById(id);
        BasicResponse basicResponse = new BasicResponse();
        basicResponse.setId(id);
        basicResponse.setTime(String.valueOf(System.currentTimeMillis() / 1000));
        if (!response.equals(HttpStatus.OK)) {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
        } else {
            basicResponse.setHasFacial(true);
            basicResponse.setHasFingers(true);
        }
        log.info("Returning: {} - {}", response, basicResponse);
        return new ResponseEntity<>(basicResponse, response);
    }

    @RequestMapping(value = "/search/fingers", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<BasicResponse> findRecordByFingers(@RequestBody RequestFingers requestFingers) 
    {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findRecordByFingers ");
        HttpStatusWrapper responseStatus = mbssUtils.findByFingers(requestFingers.getFingers(), requestFingers.getImageType());
        log.info("Buscando con datos: {}", requestFingers.getFingers());
        BasicResponse basicResponse = new BasicResponse();
        HttpStatus statusToAnswer = HttpStatus.NOT_FOUND;
        if (responseStatus.getHttpStatus().equals(HttpStatus.CONFLICT)) {
            basicResponse.setHasFacial(true);
            basicResponse.setHasFingers(true);
            basicResponse.setId(responseStatus.getId());
            statusToAnswer = HttpStatus.OK;
        } else if (responseStatus.getHttpStatus().equals(HttpStatus.PRECONDITION_FAILED)) {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
            basicResponse.setId(requestFingers.getId());
            statusToAnswer = HttpStatus.PRECONDITION_FAILED;
        } else {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
            basicResponse.setId(requestFingers.getId());
        }
        basicResponse.setTime(String.valueOf(System.currentTimeMillis() / 1000));
        log.info("Returning: {} - {}", responseStatus, basicResponse);
        return new ResponseEntity<>(basicResponse, statusToAnswer);
    }

    /**
     * Use search fingers with image type instead. Next version of the API will remove this method
     *
     * @param requestFingers
     * @return
     */
    @Deprecated
    @RequestMapping(value = "/search/fingers/jpeg", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = "application/json")
    public ResponseEntity<BasicResponse> findRecordByFingersJPEG(@RequestBody RequestFingers requestFingers) 
    {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findRecordByFingersJPEG ");
        HttpStatusWrapper responseStatus = mbssUtils.findByFingersJPEG(requestFingers.getFingers());
        BasicResponse basicResponse = new BasicResponse();
        HttpStatus statusToAnswer = HttpStatus.NOT_FOUND;
        if (responseStatus.getHttpStatus().equals(HttpStatus.CONFLICT)) {
            basicResponse.setHasFacial(true);
            basicResponse.setHasFingers(true);
            basicResponse.setId(responseStatus.getId());
            statusToAnswer = HttpStatus.OK;
        } else if (responseStatus.getHttpStatus().equals(HttpStatus.PRECONDITION_FAILED)) {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
            basicResponse.setId(requestFingers.getId());
            statusToAnswer = HttpStatus.PRECONDITION_FAILED;
        } else {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
            basicResponse.setId(requestFingers.getId());
        }
        basicResponse.setTime(String.valueOf(System.currentTimeMillis() / 1000));
        log.info("Returning: {} - {}", responseStatus, basicResponse);
        return new ResponseEntity<>(basicResponse, statusToAnswer);
    }

    @RequestMapping(value = "/search/fingersById", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<BasicResponse> findRecordByFingerAndId(@RequestBody RequestFingers requestFingers) 
    {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findRecordByFingerAndId ");
        HttpStatusWrapper responseStatus = mbssUtils.findByFingersAndId(requestFingers.getFingers(), requestFingers.getId(), requestFingers.getImageType());
        BasicResponse basicResponse = new BasicResponse();
        basicResponse.setId(responseStatus.getId());
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        if (responseStatus.getHttpStatus().equals(HttpStatus.NOT_FOUND)) {
            basicResponse.setHasFacial(false);
            basicResponse.setHasFingers(false);
        } else if (responseStatus.getHttpStatus().equals(HttpStatus.PRECONDITION_FAILED)) {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
            basicResponse.setId(requestFingers.getId());
            httpStatus = HttpStatus.PRECONDITION_FAILED;
        } else {
            httpStatus = HttpStatus.OK;
            basicResponse.setHasFacial(true);
            basicResponse.setHasFingers(true);
        }
        basicResponse.setTime(String.valueOf(System.currentTimeMillis() / 1000));
        log.info("Returning: {} - {}", responseStatus, basicResponse);
        return new ResponseEntity<>(basicResponse, httpStatus);
    }

    @RequestMapping(value = "/search/slaps", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = "application/json")
    public ResponseEntity<BasicResponse> findRecordByHands(@RequestBody RequestSlaps requestSlaps) 
    {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findRecordByHands ");
        HttpStatusWrapper responseStatus = mbssUtils.findBySlaps(requestSlaps.getSlaps());
        BasicResponse basicResponse = new BasicResponse();
        HttpStatus statusToAnswer = HttpStatus.NOT_FOUND;
        if (responseStatus.getHttpStatus().equals(HttpStatus.CONFLICT)) {
            basicResponse.setHasFacial(true);
            basicResponse.setHasFingers(true);
            basicResponse.setId(responseStatus.getId());
            statusToAnswer = HttpStatus.OK;
        } else if (responseStatus.getHttpStatus().equals(HttpStatus.PRECONDITION_FAILED)) {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
            basicResponse.setId(requestSlaps.getId());
            statusToAnswer = HttpStatus.PRECONDITION_FAILED;
        } else {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
            basicResponse.setId(requestSlaps.getId());
        }
        basicResponse.setTime(String.valueOf(System.currentTimeMillis() / 1000));
        log.info("Returning: {} - {}", responseStatus, basicResponse);
        return new ResponseEntity<>(basicResponse, statusToAnswer);
    }

    @RequestMapping(value = "/search/slapsId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = "application/json")
    public ResponseEntity<BasicResponse> findRecordByHandsAndId(@RequestBody RequestSlaps requestSlaps) 
    {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findRecordByHandsAndId ");
        HttpStatusWrapper responseStatus = mbssUtils.findBySlapsAndId(requestSlaps.getId(), requestSlaps.getSlaps());
        BasicResponse basicResponse = new BasicResponse();
        if (responseStatus.equals(HttpStatus.OK)) {
            basicResponse.setHasFacial(true);
            basicResponse.setHasFingers(true);
        } else if (responseStatus.getHttpStatus().equals(HttpStatus.PRECONDITION_FAILED)) {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
            basicResponse.setId(requestSlaps.getId());
        } else {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
        }
        basicResponse.setId(responseStatus.getId());
        basicResponse.setTime(String.valueOf(System.currentTimeMillis() / 1000));
        log.info("Returning: {} - {}", responseStatus, basicResponse);
        return new ResponseEntity<>(basicResponse, responseStatus.getHttpStatus());
    }

    @RequestMapping(value = "/search/facial", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = "application/json")
    public ResponseEntity<BasicResponse> findRecordByFacial(@RequestBody RequestFacial requestFacial) {
        log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findRecordByFacial ");
        HttpStatusWrapper responseStatus = mbssUtils.findByFace(requestFacial.getFacial());
        BasicResponse basicResponse = new BasicResponse();
        HttpStatus statusToAnswer = HttpStatus.NOT_FOUND;
        if (responseStatus.getHttpStatus().equals(HttpStatus.CONFLICT)) {
            basicResponse.setHasFacial(true);
            basicResponse.setHasFingers(true);
            basicResponse.setId(responseStatus.getId());
            statusToAnswer = HttpStatus.OK;
        } else {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
            basicResponse.setId(requestFacial.getId());
        }
        basicResponse.setTime(String.valueOf(System.currentTimeMillis() / 1000));
        log.info("Returning: {} - {}", responseStatus, basicResponse);
        return new ResponseEntity<>(basicResponse, statusToAnswer);
    }

    @RequestMapping(value = "/search/facialId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = "application/json")
    public ResponseEntity<BasicResponse> findRecordByFacialId(@RequestBody RequestFacial requestFacial) {
        log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findRecordByFacialId ");
        HttpStatusWrapper responseStatus = mbssUtils.findByFaceId(requestFacial.getId(), requestFacial.getFacial());
        BasicResponse basicResponse = new BasicResponse();
        if (responseStatus.equals(HttpStatus.OK)) {
            basicResponse.setHasFacial(true);
            basicResponse.setHasFingers(true);
        } else {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
        }
        basicResponse.setId(responseStatus.getId());
        basicResponse.setTime(String.valueOf(System.currentTimeMillis() / 1000));
        log.info("Returning: {} - {}", responseStatus, basicResponse);
        return new ResponseEntity<>(basicResponse, responseStatus.getHttpStatus());
    }

    @RequestMapping(value = "/delete/deleteById", method = RequestMethod.DELETE, produces = "application/json", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SimpleResponse> deleteById(@RequestBody String jsonRequest) {
        log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".deleteById ");
        JSONObject jsonInput = null;
        try {
            jsonInput = new JSONObject(jsonRequest);
        } catch (Exception e) {
            SimpleResponse sr = new SimpleResponse();
            sr.setStatus("NO_JSON_INPUT");
            sr.setId("0");
            sr.setTime("0");
            return new ResponseEntity<>(sr, HttpStatus.BAD_REQUEST);
        }
        String id = jsonInput.getString("id");
        Person person = mbssUtils.findDetailById(id);
        if (person == null) {
            return new ResponseEntity<>((SimpleResponse) null, HttpStatus.NOT_FOUND);
        }
        try {
            HttpStatus responseStatus = mbssUtils.deleteById(id);
            SimpleResponse response = new SimpleResponse();
            response.setId(id);
            response.setStatus(responseStatus.toString());
            response.setTime(String.valueOf(System.currentTimeMillis() / 1000));

            return new ResponseEntity<>(response, responseStatus);

        } catch (Exception e) {
            return new ResponseEntity<>((SimpleResponse) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @RequestMapping(value = "/compareFacial", method = RequestMethod.POST)
    public ResponseEntity<SimpleResponse> matchFacial(@RequestBody RequestAuthenticateFacial requestAuthenticateFacial) {
        log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".matchFacial ");
        HttpStatusImageWrapper imageWrapper = mbssUtils.compareByPhotos(requestAuthenticateFacial.getFacialB641(), requestAuthenticateFacial.getFacialB642(), requestAuthenticateFacial.getScore(), "tkn_auth_s");
        log.debug("Get from mbss: {}", imageWrapper);
        if (imageWrapper.getHttpStatus().equals(HttpStatus.OK)) {
            return new ResponseEntity<>(new SimpleResponse("1", imageWrapper.getScore(), String.valueOf(System.currentTimeMillis())), HttpStatus.OK);
        }
        return new ResponseEntity<SimpleResponse>(new SimpleResponse("0", "0", String.valueOf(System.currentTimeMillis())), HttpStatus.NOT_FOUND);
    }

    private SimpleResponse buildResponseMissingFields() 
    {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".buildResponseMissingFields ");
        return new SimpleResponse("400", "400", String.valueOf(System.currentTimeMillis()));
    }

    private SimpleResponse buildResponseBadEncodingFields() 
    {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".buildResponseBadEncodingFields ");
        return new SimpleResponse("400", "Bad encoding", String.valueOf(System.currentTimeMillis()));
    }

}
